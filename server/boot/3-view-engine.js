var path = require('path');
var exphbs = require('express-handlebars');
var loopback = require('loopback');
var moment = require('moment');

module.exports = function(app) {
    // Setup to use express-handlebars template engine **************************
    app.set('views', path.join(__dirname, '../views'));
    var hbsConfig = {
        layoutsDir: path.join(app.settings.views, 'layouts'),
        partialsDir: path.join(app.settings.views, 'partials'),
        defaultLayout: 'layout',
        extname: '.hbs',
        helpers: {
            momentFrom: function( args, options ) {
                console.log('momentFrom args ', args);
                console.log('momentFrom args[0] type is ', typeof args);
                return moment(args).fromNow();
            },
            momentCalendar: function( args, options ) {
                console.log('momentCalendar args ', args);
                console.log('momentCalendar args[0] type is ', typeof args);
                return moment(args).calendar();
            },
            helperMissing: function( /* [args, ] options */ ) {
                var options = arguments[arguments.length - 1];
                console.log('Handlebars: options is ', options);
                console.log('Handlebars: Unknown field: ' + options.name);
            }
        }
    };

    app.engine('.hbs', exphbs(hbsConfig));
    app.set('view engine', '.hbs');
};
