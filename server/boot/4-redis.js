var loopback = require('loopback');
var redis = require('redis');

module.exports = function configureRedisSession(app) {
    var redisHost;
    var redisPort;
    var redisAuth;

    if (process.env.NODE_ENV === 'development') {
        redisHost = process.env.REDIS_HOST || '127.0.0.1';
        redisPort = process.env.REDIS_PORT || 6379;
        redisAuth = process.env.REDIS_AUTH || null;
    } else {
        redisHost = process.env.REDIS_HOST || 'pub-redis-11422.us-east-1-4.6.ec2.redislabs.com';
        redisPort = process.env.REDIS_PORT || 11422;
        redisAuth = process.env.REDIS_AUTH || 'lastbites';
    }

    var client = redis.createClient(
        redisPort, // port
        redisHost, // host
        {
            auth_pass: redisAuth
        }); // options hash

    client.on('connect', function() {
        console.log('redis: connected to %s on port %s', redisHost, redisPort);
    });

    client.on('reconnecting', function() {
        console.log('redis: reconnecting');
    });

    client.on('close', function() {
        console.log('redis: closed');
    });

    client.on('end', function() {
        console.log('redis: ended');
    });

    client.on('ready', function() {
        console.log('redis: ready');
    });

    client.on('error', function(err) {
        //console.log('redis: redis err ', err);
        //console.trace('redis');
        console.error(err.stack);
    });

    var session = require('express-session');
    var RedisStore = require('connect-redis')(session);
    var flash = require('express-flash');

    app.middleware('session', loopback.bodyParser.json());
    app.middleware('session', loopback.bodyParser.urlencoded({
        extended: true
    }));

    app.middleware('session', loopback.context());
    app.middleware('session', session({
        store: new RedisStore({
            client: client
        }),
        cookie: {},
        saveUninitialized: true,
        resave: true,
        secret: app.get('cookieSecret')
    }));
    app.middleware('session', loopback.cookieParser(app.get('cookieSecret')));
    app.middleware('session', loopback.token({
        model: app.models.AccessToken
    }));
    app.middleware('session', flash());
};
