var index = require('../routes/index');
var users = require('../routes/users');
var feedItems = require('../routes/feed-items');
var businesses = require('../routes/businesses');
var auth = require('../routes/auth');
var isAuthenticated        = require('../middleware/isAuthenticated');
var addCurrentUserToLocals = require('../middleware/addCurrentUserToLocals');

module.exports = function(app) {
  app.use('/auth',       addCurrentUserToLocals(), auth);
  app.use('/businesses', addCurrentUserToLocals(), isAuthenticated('/auth/login'), businesses);
  app.use('/users',      addCurrentUserToLocals(), isAuthenticated('/auth/login'), users);
  app.use('/feed-items', addCurrentUserToLocals(), isAuthenticated('/auth/login'), feedItems);
  app.use('/',           addCurrentUserToLocals(), index);
};
