var server = require('./server');
var ds = server.dataSources.postgresDS;

//var updateTables = ['user', 'Business', 'FeedItem', UserFavorite', AccessToken', 'ACL', 'RoleMapping', 'Role', 'UserProfile'];
var updateTables = [];
//ds.automigrate(lbTables, function(er) {
var migrateTables = ['FeedItem'];

// non-destructive
ds.autoupdate(updateTables, function(er) {
  if (er) throw er;
  console.log('Looback tables [' + updateTables + '] updated in ', ds.adapter.name);
 // ds.disconnect();
});

// destructive
ds.automigrate(migrateTables, function(er) {
  if (er) throw er;
  console.log('Looback tables [' + migrateTables + '] migrated in ', ds.adapter.name);
  ds.disconnect();
});

