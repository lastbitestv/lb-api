var postgresDS = {
    'name':       'postgresDS',
    'connector':  'postgresql',
    'host':       process.env.DB_HOST     || 'lastbites-dev.cppk7tmnrlbb.us-east-1.rds.amazonaws.com',
    'port':       process.env.DB_PORT     || 5432,
    'database':   process.env.DB_DATABASE || 'lbdb',
    'username':   process.env.DB_USERNAME || 'superuser',
    'password':   process.env.DB_PASSWORD || 'lastbites',
    'debug':      process.env.DB_DEBUG    || false,
    'poolSize':   process.env.DB_POOLSIZE || 20
};

if (process.env.DB_HOST !== 'localhost') {
    postgresDS.ssl = process.env.DB_SSL || false;
}

module.exports = {
    postgresDS: postgresDS
};
