/**
 * Add current user to response locals for use in View Templates.
 *
 * @return {Function}
 * @api public
 */
module.exports = function addCurrentUserToLocals() {
    return function(req, res, next) {
        if (req.session && req.session.accessToken) {
            var token = req.session.accessToken;
            res.locals.user = token.user;
            if (req.session.categoryFilter) {
                res.locals.categoryFilter = req.session.categoryFilter;
            } else {
                res.locals.categoryFilter = null;
            }
            if (req.session.locationFilter) {
                res.locals.locationFilter = req.session.locationFilter;
            } else {
                res.locals.locationFilter = null;
            }

            res.locals.locationFilter = req.session.locationFilter;
            req.app.models.UserFavorite.count({userId:token.userId}, function(err, count) {
                if (err) {
                    console.log('addCurrentUserToLocals: error ',err);
                    delete res.locals.user;
                } else {
                    //console.log('addCurrentUserToLocals: %s has %d userFavorites', token.user.username, count);
                    res.locals.user.favoriteCount = count;
                }
                return next();
            });
        } else {
            delete res.locals.user;
            return next();
        }
    };
};
