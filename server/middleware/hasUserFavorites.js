/**
 * Ensure that a superuser is logged in currently.
 *
 * This middleware ensures that a superuser is logged in.
 *
 * @return {Function}
 * @api public
 */
var loopback = require('loopback');
module.exports = function hasUserFavorites() {
    return function(req, res, next) {
        var token = req.session.accessToken;
        console.log('hasUserFavorites: session is ', req.session);
        debugger;
        if (token && token.user) {
            console.log('hasUserFavorites: user model is ', req.app.models.prototype);
            //req.app.models.user.__count__userFavorites(function(err, count) {
            req.app.models.UserFavorite.count({userId:token.userId}, function(err, count) {
                if (err) {
                    console.log('hasUserFavorites: error ',err);
                    delete res.locals.userFavoriteCount;
                    return next(err);
                } else {
                    console.log('hasUserFavorites: %s has %d userFavorites', token.user.username, count);
                    res.locals.userFavoriteCount = count;
                    return next();
                }
            });
        } else {
            console.log('hasUserFavorites: problem: %s', token ? (token.user ? token.user.email : 'no user') : 'no token');
            return res.redirect('/');
        }
    };
};
