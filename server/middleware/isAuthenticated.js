/**
 * Ensure that a user is logged in currently.
 *
 * This middleware ensures that a user is logged in.
 *
 * @return {Function}
 * @api public
 */
module.exports = function isAuthenticated() {
    return function(req, res, next) {
        var token = req.session.accessToken;
        if (token) {
            //console.log('isAuthenticated: %s is active user', token.user.username);
            return next();
        } else {
            console.log('isAuthenticated: user is not authenticated');
            return res.redirect('/auth/login');
        }
    };
};
