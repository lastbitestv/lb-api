/**
 * Ensure that a superuser is logged in currently.
 *
 * This middleware ensures that a superuser is logged in.
 *
 * @return {Function}
 * @api public
 */
module.exports = function isSuperUser() {
    return function(req, res, next) {
        var token = req.session.accessToken;
            console.log('isSuperUser: session is ', req.session);
        if (token && token.superUser) {
            console.log('isSuperUser: %s is superUser', token.user.username);
            return next();
        } else {
            console.log('isSuperUser: is not superUser, %s', token ? (token.user ? token.user.email : 'no user') : 'no token');
            return res.redirect('/');
        }
    };
};
