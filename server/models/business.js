var loopback = require('loopback');
module.exports = function(Business) {
    // turn off a bunch of endpoint methods
    var isStatic = true;
    //Business.disableRemoteMethod('create', isStatic);
    Business.disableRemoteMethod('exists', isStatic);
    Business.disableRemoteMethod('count', isStatic);
    Business.disableRemoteMethod('createChangeStream', isStatic);
    Business.disableRemoteMethod('getChangeStream', isStatic);
    //Business.disableRemoteMethod('update', isStatic);
    Business.disableRemoteMethod('updateAll', isStatic);
    Business.disableRemoteMethod('upsert', isStatic);
    //Business.disableRemoteMethod('exists', isStatic);
    //Business.disableRemoteMethod('find', isStatic);
    //Business.disableRemoteMethod('findOne', isStatic);
    //Business.disableRemoteMethod('findById', isStatic);
    //Business.disableRemoteMethod('deleteById', isStatic);
    isStatic = false;
    Business.disableRemoteMethod('updateAttribute', isStatic);
    //Business.disableRemoteMethod('updateAttributes', isStatic);

    Business.disableRemoteMethod('__create__userFavorites', isStatic);
    //Business.disableRemoteMethod('__get__accessTokens', isStatic);
    //Business.disableRemoteMethod('__update__accessTokens', isStatic);
    Business.disableRemoteMethod('__delete__userFavorites', isStatic);
    //Business.disableRemoteMethod('__findById__accessTokens', isStatic);
    Business.disableRemoteMethod('__destroyById__userFavorites', isStatic);
    Business.disableRemoteMethod('__updateById__userFavorites', isStatic);
    //Business.disableRemoteMethod('__count__accessTokens', isStatic);


    Business.observe('after save', function(ctx, next) {
        if (ctx.instance && ctx.isNewInstance) {
            Business.app.models.FeedItem.create({
                description: 'New Business created - ' + ctx.instance.name,
                data: JSON.stringify(ctx.instance)
            }, function(err, item) {
                if (err) {
                    console.log('Business.after save: creating FeedItem had error ', err);
                }
                next();
            });
        } else {
            next();
        }
    });

    Business.isearch = function(partialName, city, category, cb) {
        var app = Business.app;
        if (!partialName) {
            return cb(null, {
                inputPhrase: partialName,
                matches: []
            });
        }
        var whereClauses = [];
        whereClauses.push("select id,name, category, subcategory, city, state from business where (lower(left(name,10)) ~~ '" + partialName.toLowerCase() + "%')");
        //SELECT id,name,city
        //FROM   business
        //WHERE  lower(city) = 'atlanta' and lower(left(name,10)) ~~ (lower('G') || '%')
        //limit 501;

        //var requestQuery = "select id,name from business where lower(city) = '" + city.toLowerCase() + "' and lower(left(name,10))~~ '" + partialName.toLowerCase() + "%'";
        if (city) {
            whereClauses.push( "(lower(city) = '" + city.toLowerCase() + "')");
        }
        if (category) {
            whereClauses.push( "(lower(category) = '" + category.toLowerCase() + "')");
        }
        var requestQuery = whereClauses.join(' and ');
        console.log('Business.isearch: query is ', requestQuery);
        var connector = Business.getDataSource().connector;

        if (connector) {
            connector.execute(requestQuery, [], function(err, records) {
                if (err) {
                    console.log('Business.isearch: execute has error ', err);
                    return cb(err);
                }
                var result = {inputPhrase: partialName, matches:records};
                if (!records) {
                    result.matches = [];
                }
                console.log('Business.isearch: returning ', result);
                return cb(null, result);
            }); // execute()
        } else {
            return cb(new Error('Business Model has no valid datasource connector'));
        }
    };

    Business.afterRemote('isearch', function(ctx, result, next) {
        console.log('after remote, result is ', result);
        if (result) {
            ctx.result = result.result;
        }
        console.log('after remote, new ctx.result is ', ctx.result);
        next();
    });

    Business.categories = function(cb) {
        var requestQuery = "select distinct category from business order by category";
        var connector = Business.getDataSource().connector;

        if (connector) {
            connector.execute(requestQuery, [], function(err, records) {
                if (err) {
                    console.log('Business.categories: execute has error ', err);
                    return cb(err);
                }
                records.unshift({category:'All Restaurants'});
                return cb(null, records);
            }); // execute()
        } else {
            return cb(new Error('Business Model has no valid datasource connector'));
        }
    };

    Business.locations = function(pattern, cb) {
        console.log('Business.locations: pattern is ', pattern);
        var requestQuery = "select distinct city as text, city as id from business";
        if (pattern) {
            requestQuery += " where lower(city) like '" + pattern.toLowerCase() + "%'";
        }
        requestQuery +=  " order by city";
        console.log('Business.locations: query is ', requestQuery);
        var connector = Business.getDataSource().connector;

        if (connector) {
            connector.execute(requestQuery, [], function(err, records) {
                if (err) {
                    console.log('Business.locations: execute has error ', err);
                    return cb(err);
                }
                records.unshift({text:'Current Location'});
                return cb(null, records);
            }); // execute()
        } else {
            return cb(new Error('Business Model has no valid datasource connector'));
        }
    };

    Business.afterRemote('categories', function(ctx, result, next) {
        console.log('after remote, result is ', result);
        if (result) {
            ctx.result = result.result;
        }
        console.log('after remote, new ctx.result is ', ctx.result);
        next();
    });

    Business.afterRemote('locations', function(ctx, result, next) {
        console.log('after remote, result is ', result);
        if (result) {
            ctx.result = result.result;
        }
        console.log('after remote, new ctx.result is ', ctx.result);
        next();
    });

    Business.remoteMethod('isearch', {
        description: 'Incrementally search businesses by name (case-insensitive)',
        http: {
            path: '/isearch',
            verb: 'get'
        },
        accepts: [
        {
            arg: 'partialName',
            type: 'String',
            required: false,
            description: "A Partial Business Name"
        }, {
            arg: 'city',
            type: 'String',
            required: false,
            description: "City where business is located"
        }, {
            arg: 'category',
            type: 'String',
            required: false,
            description: "Category of business"
        }, ],
        returns: {
            arg: "result",
            type: 'object'
        },
    });

    Business.remoteMethod('categories', {
        description: 'Get a list of business categories',
        http: {
            path: '/categories',
            verb: 'get'
        },
        returns: {
            arg: "result",
            type: 'object'
        },
    });

    Business.remoteMethod('locations', {
        description: 'Get a list of business locations',
        http: {
            path: '/locations',
            verb: 'get'
        },
        accepts:[{
            arg: 'pattern',
            type: 'String',
            required: false,
            description: "A Partial City Name"
        }],
        returns: {
            arg: "result",
            type: 'object'
        },
    });

};
