var loopback = require('loopback');
module.exports = function(FeedItem) {
    FeedItem.observe('before save', function(ctx, next) {
        if (ctx.instance) {
            var context = loopback.getCurrentContext();
            var accessToken = context.active.accessToken;
            console.log('FeedItem.before save: ctx is ', context.active);
            if (accessToken)
                ctx.instance.userId = accessToken.userId;
            next();
        } else {
            next();
        }
    });
};
