module.exports = function(UserFavorite) {
    UserFavorite.observe('after save', function(ctx, next) {
        var instance = ctx.instance;
        var User = UserFavorite.app.models.User;
        var Business = UserFavorite.app.models.Business;
        var FeedItem = UserFavorite.app.models.FeedItem;
        if (instance && ctx.isNewInstance) {
            console.log('UserFavorite after save: instance is ', instance);
            console.log('UserFavorite after save: ctx.req is ', ctx.req);
            //            var token = req.session.accessToken;
            //            res.locals.user = token.user;
            var item = {
                description: '',
                data: JSON.stringify(instance)
            };

            User.findById(instance.userId, function(err, user) {
                if (err) {
                    console.log('UserFavorite.after save: finding user by id had error ', err);
                }

                Business.findById(instance.businessId, function(err, business) {
                    if (err) {
                        console.log('UserFavorite.after save: finding business by id had error ', err);
                    }
                    if (user) {
                        item.description = "<a href='/users/"+user.id+"/favorites'>" + user.username + '</a> really likes ' + instance.item;
                    } else {
                        item.description = 'somebody really likes ' + instance.item;
                    }
                    if (business) {
                        item.description += ' at ' + "<a href='/businesses/" + business.id +"'>" + business.name + '</a> in ' + business.city + ', ' + business.state;
                    }
                    FeedItem.create(item, function(err, newItem) {
                        if (err) {
                            console.log('UserFavorite.after save: creating FeedItem had error ', err);
                        }
                        next();
                    });

                });
            });
        } else {
            next();
        }
    });
};
