/*
STATUS

you've created the user favorite and business models, but you haven't added any tests for them.
you're removing endpoint for user model that you don't want exposed. see below.
*/


module.exports = function(User) {
    var isStatic = true;

    // turn off a bunch of endpoint methods
    User.disableRemoteMethod('create', isStatic);
    User.disableRemoteMethod('exists', isStatic);
    User.disableRemoteMethod('count', isStatic);
    User.disableRemoteMethod('createChangeStream', isStatic);
    User.disableRemoteMethod('getChangeStream', isStatic);
    //User.disableRemoteMethod('update', isStatic);
    User.disableRemoteMethod('updateAll', isStatic);
    User.disableRemoteMethod('upsert', isStatic);
    //User.disableRemoteMethod('exists', isStatic);
    User.disableRemoteMethod('find', isStatic);
    User.disableRemoteMethod('findOne', isStatic);
    //User.disableRemoteMethod('findById', isStatic);
    //User.disableRemoteMethod('deleteById', isStatic);
    isStatic = false;
    User.disableRemoteMethod('__create__accessTokens', isStatic);
    User.disableRemoteMethod('__get__accessTokens', isStatic);
    User.disableRemoteMethod('__update__accessTokens', isStatic);
    User.disableRemoteMethod('__delete__accessTokens', isStatic);
    User.disableRemoteMethod('__findById__accessTokens', isStatic);
    User.disableRemoteMethod('__destroyById__accessTokens', isStatic);
    User.disableRemoteMethod('__updateById__accessTokens', isStatic);
    User.disableRemoteMethod('__count__accessTokens', isStatic);
    User.disableRemoteMethod('updateAttribute', isStatic);
    //User.disableRemoteMethod('updateAttributes', isStatic);

    User.observe('after save', function(ctx, next) {
        var instance = ctx.instance;
        if (instance && ctx.isNewInstance) {
            delete instance.password;
            console.log('user after save: instance is ', instance);
            User.app.models.FeedItem.create({
                description: 'Welcome ' + "<a href='/users/"+instance.id+"/favorites'>" + instance.username + '</a>, our newest user!',
                data: JSON.stringify(instance)
            }, function(err, item) {
                if (err) {
                    console.log('User.after save: creating FeedItem had error ', err);
                }
                next();
            });
        } else {
            next();
        }
    });



    User.delete = function(id, cb) {
        var app = User.app;
        User.findById(id, function(err, user) {
            if (err) {
                console.log('User.delete: finding user got error ', err);
                return cb(err);
            } else {
                // need to convert the result to an object to remove related mode functions
                var u = user.toObject(false, false, false);
                if (u.profile && u.profile.id) {
                    // destroy profile first
                    app.models.UserProfile.destroyById(u.profile.id, function(err) {
                        if (err) {
                            console.log('User.delete: deleting user profile got error ', err);
                            return cb(err);
                        } else {
                            // now destroy user
                            User.destroyById(u.id, function(err) {
                                if (err) {
                                    console.log('User.delete: deleting user got error ', err);
                                } else {
                                    return cb();
                                }
                            });
                        }
                    });
                } else {
                    // no profile exists, so just destroy user
                    User.destroyById(u.id, function(err) {
                        if (err) {
                            console.log('User.delete: deleting user got error ', err);
                        }
                        return cb(err);
                    });
                }
            }
        });
    };

    User.register = function(username, password, email, handle, firstName, lastName, city, cb) {
        var app = User.app;
        var userObject = {
            username: username,
            password: password,
            email: email
        };
        var userProfileObject = {
            handle: handle,
            firstName: firstName,
            lastName: lastName,
            city: city
        };

        User.create(userObject, function(err, newUser) {
            if (err) {
                console.log('User.register: creating new user got error ', err);
                return cb(err);
            } else {
                console.log('User.register: newUser is ', newUser);
                userProfileObject.userId = newUser.id;
                app.models.UserProfile.create(userProfileObject, function(err, newUserProfile) {
                    if (err) {
                        console.log('User.register: creating new user got error ', err);
                        console.log('User.register: destroying User id ', newUser.id);
                        User.destroyById(newUser.id, function(destroyErr) {
                            return cb(err); // return original error that caused creation to fail.
                        });
                    } else {
                        console.log('User.register: newUserProfile is ', newUserProfile);
                        User.findById(newUser.id, {
                            include: "profile"
                        }, function(err, foundUser) {
                            if (err) {
                                console.log('User.register: finding created user got error ', err);
                                return cb(err);
                            } else {
                                console.log('User.register: returning ', foundUser);
                                return cb(err, foundUser);
                            }
                        });
                    }
                });
            }
        });
    };

    User.remoteMethod('register', {
        description: 'Register a new user',
        notes: 'Includes the user and user profile',
        http: {
            path: '/register',
            verb: 'post'
        },
        returns: {
            arg: 'user',
            type: 'object'
        },
        accepts: [{
            arg: 'username',
            type: 'String',
            required: true,
            description: "A unique username"
        }, {
            arg: 'password',
            type: 'String',
            required: true,
            description: "A password"
        }, {
            arg: 'email',
            type: 'String',
            required: true,
            description: "A unique email address"
        }, {
            arg: 'handle',
            type: 'String',
            required: true,
            description: "A unique, publicly visible user handle"
        }, {
            arg: 'firstName',
            type: 'String',
            required: false,
            description: "First Name"
        }, {
            arg: 'lastName',
            type: 'String',
            required: false,
            description: "Last Name"
        }, {
            arg: 'city',
            type: 'String',
            required: false,
            description: "Where do you live?"
        }]
    });
    User.remoteMethod('delete', {
        description: 'Delete a user',
        notes: 'Removes the user and the user profile',
        http: {
            path: '/delete',
            verb: 'post',
            status: 204
        },
        accepts: [{
            arg: 'id',
            type: 'String',
            required: true,
            description: "The User Id"
        }]
    });
};
