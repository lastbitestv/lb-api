//var loopback = require('loopback');
var redis = require('redis');

//var redisHost = process.env.REDIS_HOST || 'pub-redis-11422.us-east-1-4.6.ec2.redislabs.com';
//var redisPort = process.env.REDIS_PORT || 11422;
//var redisAuth = process.env.REDIS_AUTH || 'lastbites';
var redisHost = process.env.REDIS_HOST || '127.0.0.1';
var redisPort = process.env.REDIS_PORT || 6379;
var redisAuth = process.env.REDIS_AUTH || null;

var client = redis.createClient(
    redisPort, // port
    redisHost, // host
    {
        auth_pass: redisAuth
    }); // options hash
console.log('myredis');
client.on('connect', function() {
    console.log('myredis: redis connected to %s on %s', redisHost, redisPort);
});

client.on('close', function() {
    console.log('myredis: redis connection closed');
});

client.on('ready', function() {
    console.log('myredis: redis ready');
});

client.on('error', function(err) {
    console.log('myredis: redis err ', err);
});

module.exports = client;
