var loopback = require('loopback');
var router = loopback.Router();

/* GET login page. */
router.get('/login', function(req, res) {
    res.render('pages/login', {
        hideLogin: true
    });
});

/* POST login page. */
router.post('/login', function(req, res) {
    console.log('POST /auth/login: attempt to login user');
    var user = {
        username: req.body.username,
        password: req.body.password
    };
    if (user.username) {
        user.username = user.username.toLowerCase().trim();
        req.app.models.user.login(user, "user", function(err, accessToken) {
            if (err) {
                console.log('POST /auth/login: error logging in user %s, %s ', user.username, err.message);
                req.flash('error', err.message);
                return res.redirect('back');
            }

            // keep the accessToken in the current session.
            req.session.accessToken = accessToken;
            req.accessToken = accessToken;
            req.isAuthenticated = true;
            return res.redirect('/');
        });
    } else {
        // no username
        console.log('POST /auth/login: error, no username');
        req.flash('error', 'please enter a username and password');
        return res.redirect('back');
    }
});

/* POST logout page. */
router.get('/logout', function(req, res) {
    var token = req.session.accessToken || req.accessToken;
    if (req.session.accessToken) {
        delete req.session.accessToken;
    }
    if (req.accessToken) {
        delete req.accessToken;
    }
    req.isAuthenticated = false;

    req.session.destroy(function(err) {
        if (err) {
            console.log('GET /auth/logout: session.destory had error ', err);
        }
        if (token) {
            console.log('GET /auth/logout: token is ', token);
            var tokenId = token.id;
            var userId = token.userId;
            req.app.models.user.logout(tokenId, function(err) {
                if (err) {
                    console.log('GET /auth/logout: err is ', err);
                }
                return res.redirect('/');
            });
        } else {
            console.log('GET /auth/logout: no token, logging out');
            return res.redirect('/');
        }
    });

});

/* GET signup page. */
router.get('/signup', function(req, res) {
    res.render('pages/signup');
});

/* POST signup page. */
router.post('/signup', function(req, res) {
    var app = req.app;
    var User = app.models.user;
    var frm = req.body.newUser;
    console.log('POST /signup body is ', frm);
    var password = frm.password;
    var confirmPassword = frm.confirmPassword;
    if (password === confirmPassword) {
        User.register(frm.username, frm.password, frm.email, frm.handle, frm.firstName, frm.lastName, frm.city, function(err, newUser) {
            if (err) {
                console.log('POST /signup: error registering new user ', err);
                req.flash('error', err.message);
                res.render('pages/user-add', {
                    newUser: frm
                });
            } else {
                console.log('POST /signup: registered new user ', newUser);
                req.flash('info', 'new user registered');
                res.redirect('/users');
            }
        });
    } else {
        req.flash('error', 'please reenter your password');
        res.render('pages/user-add', {
            newUser: frm
        });
    }
});

/* GET forgot password page. */
router.get('/forgot-password', function(req, res) {
    res.render('pages/forgot-password');
});

/* POST forgot password page. */
router.post('/forgot-password', function(req, res) {
    req.flash('info', 'password reminder sent');
    res.redirect('/');
});

module.exports = router;
