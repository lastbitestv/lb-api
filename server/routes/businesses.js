var loopback = require('loopback');
var router = loopback.Router();

var dumpState = function(msg, req, res) {
    if (req.session) {
        console.log('%s: session is ', msg, req.session);
    } else {
        console.log('%s: no session', msg);
    }
    console.log('%s: cookies is ', msg, req.cookies);
    console.log('%s: signed cookies is ', msg, req.signedCookies);
};

router.get('/filterCategory', function(req, res) {
    console.log('GET /businesses/filterCategory');
    var app = req.app;
    var Business = app.models.Business;

    Business.categories(function(err, categories) {
        if (err) {
            console.log('GET /businesses/filterCategory: err is ', err);
            res.render('pages/business-filter-category', {
                categories: []
            });
        } else {
            console.log('GET /businesses/filterCategory: categories is ', categories);
            res.render('pages/business-filter-category', {
                categories: categories
            });
        }
    });
});

router.post('/filterCategory', function(req, res) {
    var app = req.app;
    var Business = app.models.Business;
    var category = req.body.category;
    if (category === 'All Restaurants') {
        delete req.session.categoryFilter;
    } else {
        req.session.categoryFilter = req.body.category;
    }

    res.redirect('/users/' + res.locals.user.id + '/favorites/add');
});

router.get('/filterLocation', function(req, res) {
    console.log('GET /businesses/filterLocation');
    var app = req.app;
    var Business = app.models.Business;
    res.render('pages/business-filter-location');

    /*    Business.locations(function(err, locations) {
            if (err) {
                console.log('GET /businesses/filterLocation: err is ', err);
                res.render('pages/business-filter-location', {
                    locations: []
                });
            } else {
                console.log('GET /businesses/filterLocation: locations is ', locations);
                res.render('pages/business-filter-location', {
                    locations: locations
                });
            }
        });*/
});

router.post('/filterLocation', function(req, res) {
    var app = req.app;
    var Business = app.models.Business;
    var location = req.body.location;
    console.log('POST /filterLocation: req.body is ', req.body);
    if (location === 'Current Location') {
        delete req.session.locationFilter;
    } else {
        req.session.locationFilter = req.body.location;
    }

    res.redirect('/users/' + res.locals.user.id + '/favorites/add');
});

router.get('/add', function(req, res) {
    console.log('GET /businesses/add/');
    //dumpState('GET /businesses/add', req, res);

    res.render('pages/business-add');
});

router.post('/add', function(req, res) {
    var app = req.app;
    var Business = app.models.Business;
    var input = req.body.newBusiness;
    //dumpState('POST /businesses/add', req, res);

    console.log('POST /businesses/add body is ', input);
    Business.create(input, function(err, newBusiness) {
        if (err) {
            console.log('POST /businesses/add: error adding new business ', err);
            req.flash('error', err.message);
            res.render('pages/business-add', {
                newBusiness: input
            });
        } else {
            console.log('POST /businesses/add: added new business ', newBusiness);
            req.flash('info', 'new business added');
            res.redirect('/businesses');
        }
    });
});

router.post('/del', function(req, res) {
    var app = req.app;
    var Business = app.models.Business;
    var id = req.body.businessid;
    Business.destroyById(id, function(err) {
        if (err) {
            console.log('POST /businesses/del id %s err ', id, err);
            res.sendStatus(404);
        } else {
            req.flash('info', 'business deleted');
            res.redirect('/businesses');
        }
    });
});

router.get('/:id', function(req, res) {
    var app = req.app;
    var Business = app.models.Business;
    var id = req.params.id;
    console.log('GET /businesses/:id, id=', id);

    //dumpState('get /businesses/:id', req, res);
    Business.findById(id, function(err, business) {
        if (err) {
            console.log('finding business by id got error ', err);
            req.flash('error', 'error finding business by id');
            res.render('pages/business-detail', {
                title: 'Business',
                business: null
            });
        } else {
            console.log('found business ', business);
            res.render('pages/business-detail', {
                title: 'Business',
                business: business
            });
        }
    });
});

router.get('/:id/favorites', function(req, res) {
    var app = req.app;
    var Business = app.models.Business;
    var id = req.params.id;
    console.log('GET /businesses/:id, id=', id);

    Business.findById(id, {
        include: {
            userFavorites: 'user'
        }
    }, function(err, business) {
        if (err) {
            console.log('finding business by id got error ', err);
            req.flash('error', 'error finding business by id');
            res.render('pages/business-detail', {
                title: 'Business',
                business: null
            });
        } else {
            if (business) {
                console.log('got business (raw)', business);
                var b2 = business.toObject(false, false, false);
                console.log('got business(toObject) ', b2);

                return res.render('pages/business-favorites-index', {
                    title: 'Business Favorites',
                    business: b2,
                    favorites: b2.userFavorites
                });
            } else {
                console.log('GET /businesses/favorites no business found');
                req.flash('info', 'GET /businesses/favorites no business found');
                return res.render('pages/business-favorites-index', {
                    title: 'Business Favorites',
                    business: null,
                    favorites: null
                });
            }
        }
    });






    //dumpState('get /businesses/:id', req, res);
/*    Business.findById(id, function(err, business) {
        if (err) {
            console.log('finding business by id got error ', err);
            req.flash('error', 'error finding business by id');
            res.render('pages/business-detail', {
                title: 'Business',
                business: null
            });
        } else {
            console.log('found business ', business);
            res.render('pages/business-detail', {
                title: 'Business',
                business: business
            });
        }
    }); */
});

/* GET businesses listing. */
router.get('/', function(req, res) {
    console.log('GET /businesses/');
    var app = req.app;
    var Business = app.models.Business;
    //dumpState('get /businesses', req, res);
    Business.find(function(err, businesses) {
        if (err) {
            console.log('finding businesses got error ', err);
            req.flash('error', 'error finding businesses');
            res.render('pages/business-index', {
                title: 'Business List',
                businesses: []
            });
        } else {
            console.log('found businesses ', businesses);
            res.render("pages/business-index", {
                title: 'Business List',
                businesses: businesses
            });
        }
    });
});

module.exports = router;
