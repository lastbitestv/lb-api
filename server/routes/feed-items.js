var loopback = require('loopback');
var router = loopback.Router();

router.get('/:id', function(req, res) {
  var app = req.app;
  var FeedItem = app.models.FeedItem;
  var id = req.params.id;
  console.log('get /feed-items/:id, id=',id);

  FeedItem.findById(id, function(err,feedItem){
    if (err) {
      console.log('finding feed item by id got error ', err);
      req.flash('error','error finding feed item by id');
      res.render('pages/feed-item-detail', {title: 'Feed Item', feedItem: null});
    } else {
      console.log('found feed item ', feedItem);
      res.render('pages/feed-item-detail', {title: 'Feed Item', feedItem: feedItem});
    }
  });
});

/* GET businesses listing. */
router.get('/', function(req, res) {
  var app = req.app;
  var FeedItem = app.models.FeedItem;
  FeedItem.find(function(err,feedItems){
    if (err) {
      console.log('finding feedItems got error ', err);
      req.flash('error','error finding feedItems');
      res.render('pages/feed-item-index', {title: 'Feed Item List', feedItems: []});
    } else {
      console.log('found businesses ', feedItems);
      res.render("pages/feed-item-index", {title: 'Feed Item List', feedItems: feedItems});
    }
  });
});

module.exports = router;
