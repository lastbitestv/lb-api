var uuid = require('node-uuid');
var pg = require('pg');

exports.index = function(req, res) {
    pg.connect(req.app.get('dbConfig'), function(err, client, done) {
        var rows = [];
        var query = client.query('SELECT * FROM HIKES');
        query.on('row', function(row) {
            console.log('query on row = ',row);
            rows.push(row);
        });
        query.on('end', function(){
            console.log('query on end');
            console.log(JSON.stringify(rows));
            res.render('hike', {title: 'My Hiking Log', hikes: rows});
            done();
        });
        client.on('error', function(error) {
          console.log('client on error = ',error);
          done();
        });
        query.on('error', function(error) {
          console.log('client on error = ',error);
          res.send(error);
          done();
        });
    });
};

exports.addHike = function(req, res){
    var input = req.body.hike;
    console.log('add_hike body is ', req.body);
    var hike = { HIKE_DATE: new Date(), ID: uuid.v4(), NAME: input.NAME,
        LOCATION: input.LOCATION, DISTANCE: input.DISTANCE, WEATHER: input.WEATHER};

    console.log('Request to log hike:' + JSON.stringify(hike));
    pg.connect(req.app.get('dbConfig'), function(err, client, done) {
        //call `done()` to release the client back to the pool
        if(err) {
            done();
            return console.error('error fetching client from pool', err);
        }
        client.query('INSERT INTO HIKES (ID, HIKE_DATE,NAME,DISTANCE,LOCATION,WEATHER) values($1, $2, $3, $4, $5, $6)',
                    [uuid.v4(), hike.HIKE_DATE,hike.NAME,hike.DISTANCE,hike.LOCATION,hike.WEATHER],
                    function(err) {
                        done();
                        if (err) {
                            res.send(err);
                        } else {
                            res.redirect('/hikes');
                        }
                    });
    });
};
