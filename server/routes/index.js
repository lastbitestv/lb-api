var loopback = require('loopback');
var router = loopback.Router();

var dumpState = function(msg, req, res) {
    if (req.session) {
        console.log('%s: session is ', msg, req.session);
    } else {
        console.log('%s: no session', msg);
    }
    console.log('%s: cookies is ', msg, req.cookies);
    console.log('%s: signed cookies is ', msg, req.signedCookies);
};

/* GET home page. */
router.get('/', function(req, res) {
    dumpState('GET /', req, res);
    res.render('pages/index', {
        title: 'Last Bites'
    });
});

module.exports = router;
