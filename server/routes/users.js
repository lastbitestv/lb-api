var loopback = require('loopback');
var router = loopback.Router();

var dumpState = function(msg, req, res) {
    if (req.session) {
        console.log('%s: session is ', msg, req.session);
    } else {
        console.log('%s: no session', msg);
    }
    console.log('%s: cookies is ', msg, req.cookies);
    console.log('%s: signed cookies is ', msg, req.signedCookies);
};

router.get('/add', function(req, res) {
    dumpState('GET /users/add', req, res);
    res.render('pages/user-add');
});

router.post('/add', function(req, res) {
    dumpState('POST /users/add', req, res);
    var app = req.app;
    var User = app.models.user;
    var frm = req.body.newUser;
    console.log('POST /users/add body is ', frm);
    var password = frm.password;
    var confirmPassword = frm.confirmPassword;
    if (password === confirmPassword) {
        User.register(frm.username, frm.password, frm.email, frm.handle, frm.firstName, frm.lastName, frm.city, function(err, newUser) {
            if (err) {
                console.log('POST /users/add: error registering new user ', err);
                req.flash('error', err.message);
                res.render('pages/user-add', {
                    newUser: frm
                });
            } else {
                console.log('POST /users/add: registered new user ', newUser);
                req.flash('info', 'new user registered');
                res.redirect('/users');
            }
        });
    } else {
        req.flash('error', 'please reenter your password');
        res.render('pages/user-add', {
            newUser: frm
        });
    }
});

/* GET change password page. */
router.get('/change-password', function(req, res) {
    res.render('pages/change-password');
});

/* POST change password page. */
router.post('/change-password', function(req, res) {
    req.flash('info', 'password changed');
    res.redirect('/');
});

router.get('/:id', function(req, res) {
    var app = req.app;
    var User = app.models.user;
    var id = req.params.id;

    dumpState('get /users/:id', req, res);
    User.findById(id, {
        include: 'profile'
    }, function(err, user) {
        if (err) {
            console.log('finding user by id got error ', err);
            req.flash('error', 'error finding user by id');
            res.render('pages/user-detail', {
                title: 'User',
                user: null
            });
        } else {
            if (user) {
                console.log('got user (raw)', user);
                var u = user.toJSON();
                var u2 = user.toObject(false, false, false);
                console.log('got user (toJson)', u);
                console.log('got user (toObject) ', u2);

                res.render('pages/user-detail', {
                    title: 'User',
                    user: u,
                    profile: u.profile
                });
            } else {
                res.redirect('/users');
            }
        }
    });
});

router.post('/del', function(req, res) {
    var app = req.app;
    var User = app.models.user;
    var id = req.body.userid;
    User.delete(id, function(err) {
        if (err) {
            console.log('POST /users/del id %s err ', id, err);
            res.sendStatus(404);
        } else {
            req.flash('info', 'user deleted');
            res.redirect('/users');
        }
    });
});

/* GET a specific user favorite. */
router.get('/:userid/favorites/add', function(req, res) {
    var userId = req.params.userid;
    dumpState('GET /users/favorites/add', req, res);
    res.render('pages/user-favorite-add', {newItem:{userId:userId}});
});

/* POST a specific user favorite. */
router.post('/:userid/favorites/add', function(req, res) {
    var app = req.app;
    var User = app.models.user;
    var UserFavorite = app.models.UserFavorite;
    var userId = req.params.userid;
    var favoriteId = req.params.favoriteid;
    var input = req.body.newItem;
    console.log('POST /users/favorites/add body is ', input);
    if (input.businessId) {
        UserFavorite.create(input, function(err, newItem) {
            if (err) {
                console.log('POST /users/favorites/add: error creating new user favorite ', err);
                req.flash('error', err.message);
                res.render('pages/user-favorite-add', {
                    newItem: input
                });
            } else {
                console.log('POST /users/favorites/add: registered new user favorite ', newItem);
                res.redirect('/users/'+userId+'/favorites');
            }
        });
    } else {
        console.log('POST /users/favorites/add: businessId is blank ');
        input.businessId = '';
        req.flash('error', 'Please select a business');
        res.render('pages/user-favorite-add', {
            newItem: input
        });
    }
});

/* POST a specific user favorite. */
router.post('/:userid/favorites/:favoriteid/del', function(req, res) {
    var app = req.app;
    var User = app.models.user;
    var UserFavorite = app.models.UserFavorite;
    var userId = req.params.userid;
    var favoriteId = req.params.favoriteid;
    dumpState('get /users/favorites/del', req, res);
    console.log('GET /users/favorites/:favoriteid/del userId = ', userId);
    console.log('GET /users/favorites/:favoriteid/del favoriteId = ', favoriteId);

    UserFavorite.destroyById(favoriteId, function(err) {
        if (err) {
            console.log('deleting userFavorite by id got error ', err);
            req.flash('error', 'error deleting userFavorite by id');
            return res.render('pages/user-detail', {
                title: 'User',
                user: null
            });
        } else {
            console.log('deleted favorite');
            req.flash('info', 'user favorite deleted');
            return res.redirect('/users/'+userId+'/favorites');
        }
    });
});

/* GET a specific user favorite. */
router.get('/:userid/favorites/:favoriteid', function(req, res) {
    var app = req.app;
    var User = app.models.user;
    var UserFavorite = app.models.UserFavorite;
    var userId = req.params.userid;
    var favoriteId = req.params.favoriteid;
    dumpState('get /users/favorites', req, res);
    console.log('GET /users/favorites/:favoriteid userId = ', userId);
    console.log('GET /users/favorites/:favoriteid favoriteId = ', favoriteId);

    UserFavorite.findById(favoriteId, {
        include: 'business'
    }, function(err, favorite) {
        if (err) {
            console.log('finding userFavorite by id got error ', err);
            req.flash('error', 'error finding userFavorite by id');
            return res.render('pages/user-detail', {
                title: 'User',
                user: null
            });
        } else {
            if (favorite) {
                console.log('got favorite (raw)', favorite);
                var f2 = favorite.toObject(false, false, false);
                console.log('got favorite (toObject) ', f2);

                return res.render('pages/user-favorite-detail', {
                    title: 'User Favorite',
                    favorite: f2
                });
            } else {
                console.log('GET /users/favorites/:favoriteid no favorite found');
                req.flash('info', 'GET /users/favorites/:favoriteid no favorite found');
                return res.redirect('/users');
            }
        }
    });
});

/* GET userFavorites index */
router.get('/:userid/favorites', function(req, res) {
    var app = req.app;
    var User = app.models.user;
    var UserFavorite = app.models.UserFavorite;
    var userId = req.params.userid;
    dumpState('get /users/favorites', req, res);
    console.log('GET /users/favorites userId = ', userId);

    //Post.find({include: {owner: ‘orders’}}, function() { ... });
    User.findById(userId, {
        include: {
            userFavorites: 'business'
        }
    }, function(err, user) {
        if (err) {
            console.log('finding user by id got error ', err);
            req.flash('error', 'error finding user by id');
            return res.render('pages/user-detail', {
                title: 'User',
                user: null
            });
        } else {
            if (user) {
                console.log('got user (raw)', user);
                var u2 = user.toObject(false, false, false);
                console.log('got user (toObject) ', u2);

                return res.render('pages/user-favorites-index', {
                    title: 'User Favorites',
                    user: u2,
                    favorites: u2.userFavorites
                });
            } else {
                console.log('GET /users/favorites no user found');
                req.flash('info', 'GET /users/favorites no user found');
                return res.redirect('/users');
            }
        }
    });
});

/* GET users listing. */
router.get('/', function(req, res) {
    var app = req.app;
    var User = app.models.user;
    dumpState('get /users', req, res);
    User.find({
        include: 'profile'
    }, function(err, users) {
        if (err) {
            console.log('finding users got error ', err);
            req.flash('error', 'error finding users');
            res.render('pages/user-index', {
                title: 'User List',
                users: []
            });
        } else {
            console.log('found users ', users);
            res.render('pages/user-index', {
                title: 'User List',
                users: users
            });
        }
    });
});

module.exports = router;
