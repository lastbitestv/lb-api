console.log('\n');
console.log(' __          ___           _______.___________..______    __  .___________. _______     _______.');
console.log('|  |        /   \\         /       |           ||   _  \\  |  | |           ||   ____|   /       |');
console.log('|  |       /  ^  \\       |   (----`---|  |----`|  |_)  | |  | `---|  |----`|  |__     |   (----`');
console.log('|  |      /  /_\\  \\       \\   \\       |  |     |   _  <  |  |     |  |     |   __|     \\   \\    ');
console.log('|  `----./  _____  \\  .----)   |      |  |     |  |_)  | |  |     |  |     |  |____.----)   |   ');
console.log('|_______/__/     \\__\\ |_______/       |__|     |______/  |__|     |__|     |_______|_______/    ');
console.log('\n');

var loopback = require('loopback');
var boot = require('loopback-boot');

var app = module.exports = loopback();

app.start = function() {
    console.log('app.start()');
  // start the web server
  return app.listen(function() {
    app.emit('started');
    process.once('SIGUSR2', function () {
      console.log('\nrestarting, do app specfic stuff here\n');
//      gracefulShutdown(function () {
        process.kill(process.pid, 'SIGUSR2');
//      });
    });
    console.log('Web server listening at: %s', app.get('url'));
  });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
    console.log('boot callback()');
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module)
    app.start();
});

