/* globals describe, it, before, after */
var chakram = require('chakram');
var expect = chakram.expect;
describe("Business", function() {
    var validUserAccessToken = "";
    var newBusinessId = "";

    var createBusinessResponse;

    before(function() {
      var validUserFields = {
        username: "markl",
        password: "markl"
      };
      return chakram.post("http://localhost:3000/api/users/login",validUserFields)
      .then(function(response){
        validUserAccessToken = response.body.id;
        return chakram.wait();
      });
    });

    it("Create a Business ", function() {
      var timestamp = Date.now().toString();
      var newBusinessObject = {
        name : "testbusiness-"+timestamp
      };
      var url = "http://localhost:3000/api/businesses?access_token="+validUserAccessToken;
      return chakram.post(url,newBusinessObject)
      .then(function createBusinessSuccess(response) {
        expect(response).to.have.status(200);
        newBusinessId = response.body.id;
        var url  = "http://localhost:3000/api/businesses/" + newBusinessId + "?access_token="+validUserAccessToken;
        return chakram.delete(url);
      }, function createBusinessError(error){
        console.log('createBusinessError, error is ', error);
        return chakram.wait();
      })
      .then(function deleteBusinessSuccess(response){
        expect(response).to.have.status(204);
        return chakram.wait();
      }, function deleteBusinessError(error){
        console.log('deleteBusinessError, error is ', error);
        return chakram.wait();
      });
    });

    after(function() {
      chakram.post("http://localhost:3000/api/users/logout?access_token="+validUserAccessToken);
      return chakram.wait();
    });
});
