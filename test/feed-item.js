/* globals describe, it, before, after */
var chakram = require('chakram');
var expect = chakram.expect;
describe("Feed Item", function() {
    var validUserAccessToken = "";
    var newObjectId = "";

    before(function() {
      var validUserFields = {
        username: "markl",
        password: "markl"
      };
      return chakram.post("http://localhost:3000/api/users/login",validUserFields)
      .then(function(response){
        validUserAccessToken = response.body.id;
        return chakram.wait();
      });
    });

    it("Create a Feed Item ", function() {
      var timestamp = Date.now().toString();
      var newObject = {
        description : "feed items contents : "+timestamp,
        data: JSON.stringify({
            field1:"value1",
            field2: 2,
            field3: 3/3/3
        })
      };
      var url = "http://localhost:3000/api/feeditems?access_token="+validUserAccessToken;
      return chakram.post(url,newObject)
      .then(function createFeedSuccess(response) {
        expect(response).to.have.status(200);
        newObjectId = response.body.id;
        var url  = "http://localhost:3000/api/feeditems/" + newObjectId + "?access_token="+validUserAccessToken;
        return chakram.wait();
      });
    });

    after(function() {
      chakram.post("http://localhost:3000/api/users/logout?access_token="+validUserAccessToken);
      return chakram.wait();
    });
});
