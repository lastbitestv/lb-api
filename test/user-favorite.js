/* globals describe, it, before, after */
var chakram = require('chakram');
var expect = chakram.expect;
describe("UserFavorite", function() {
    var validUserAccessToken = "";
    var validUserId = "";
    var newObjectId = "";

    var createResponse;

    before(function() {
      var validUserFields = {
        username: "markl",
        password: "markl"
      };
      return chakram.post("http://localhost:3000/api/users/login",validUserFields)
      .then(function(response){
        validUserAccessToken = response.body.id;
        validUserId = response.body.userId;
        return chakram.wait();
      });
    });

    it("Create a UserFavorite ", function() {
      var timestamp = Date.now().toString();
      var newObject = {
        item : "testitem-"+timestamp,
        userId : validUserId,
        category : "Mexican",
        businessId : "4"
      };
      var url = "http://localhost:3000/api/userfavorites?access_token="+validUserAccessToken;
      return chakram.post(url,newObject)
      .then(function createSuccess(response) {
        expect(response).to.have.status(200);
        newObjectId = response.body.id;
        var url  = "http://localhost:3000/api/userfavorites/" + newObjectId + "?access_token="+validUserAccessToken;
        return chakram.delete(url);
      }, function createError(error){
        console.log('createError, error is ', error);
        return chakram.wait();
      })
      .then(function deleteSuccess(response){
        expect(response).to.have.status(204);
        return chakram.wait();
      }, function deleteError(error){
        console.log('Create a UserFavorite error is ', error);
        return chakram.wait();
      });
    });

    after(function() {
      chakram.post("http://localhost:3000/api/users/logout?access_token="+validUserAccessToken);
      return chakram.wait();
    });
});
