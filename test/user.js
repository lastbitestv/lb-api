/* globals describe, it, before, after */
var chakram = require('chakram'); // chakram is the API testing framework
var expect = chakram.expect;
describe("User", function() {
  describe("Login/Logout", function() {
    var validUserFields = {
      username: "markl",
      password: "markl"
    };
    var validUserModel;
    var validUserPost;
    var validUserAccessToken = "";
    var validUserId = "";
    var validUserProfile;

    it("Can login:", function() {
      validUserPost = chakram.post("http://localhost:3000/api/users/login",validUserFields);
      expect(validUserPost).to.have.status(200);
      return chakram.wait();
    });

    it("  • returning an access token;", function() {
      expect(validUserPost).to.have.json('id',function(id){
        validUserAccessToken = id;
        expect(id).to.be.a('string');
      });
    });

    it("  • and returning a userId;", function() {
      expect(validUserPost).to.have.json('userId',function(userId){
        validUserId = userId;
        expect(userId).to.be.a('string');
      });
    });

    it("  • and setting a session cookie.", function() {
      expect(validUserPost).to.have.cookie('connect.sid');
    });

    it("Can logout", function() {
      var logoutPost = chakram.post("http://localhost:3000/api/users/logout?access_token="+validUserAccessToken);
      expect(logoutPost).to.have.status(204);
      return chakram.wait();
    });
  });

  describe("Account", function() {
    var validUserFields = {
      username: "markl",
      password: "markl"
    };
    var validUserAccessToken = "";
    var validUserId = "";
    var changeUserResponse;
    var originalEmail = "markl@gmail.com";
    var originalUsername = "markl";
    var newUsername = "zaphod";
    var newEmail = "zaphod@gmail.com";
    var changeUserURL;
    var timestamp = Date.now().toString();

    this.timeout(15000);

    before(function() {
      return chakram.post("http://localhost:3000/api/users/login",validUserFields)
      .then(function(response){
        validUserAccessToken = response.body.id;
        validUserId = response.body.userId;
        return chakram.wait();
      });
    });

    it("Can change their username", function() {
      changeUserURL = "http://localhost:3000/api/users/"+validUserId+"?access_token="+validUserAccessToken;
      var newUsernameObject = {
        username:newUsername
      };
      return chakram.put(changeUserURL,newUsernameObject)
      .then(function(response){
        expect(response).to.have.status(200);
        return chakram.wait();
      });
    });

    it("Can change their email address", function() {
      changeUserURL = "http://localhost:3000/api/users/"+validUserId+"?access_token="+validUserAccessToken;
      var newEmailObject = {
        email:newEmail
      };
      return chakram.put(changeUserURL,newEmailObject)
      .then(function(changeUserResponse){
        expect(changeUserResponse).to.have.status(200);
        return chakram.wait();
      });
    });

    after(function() {
      var originalObject = {
        username:originalUsername,
        email:originalEmail
      };
      changeUserURL = "http://localhost:3000/api/users/"+validUserId+"?access_token="+validUserAccessToken;
      return chakram.put(changeUserURL,originalObject)
      .then(function (response){
        chakram.post("http://localhost:3000/api/users/logout?access_token="+validUserAccessToken);
        return chakram.wait();
      });
    });
  });

  describe("UserProfile", function() {
    var validUserFields = {
      username: "markl",
      password: "markl"
    };
    var validUserAccessToken = "";
    var validUserId = "";
    var validUserProfile;
    var validUserProfileId;

    before(function() {
      return chakram.post("http://localhost:3000/api/users/login",validUserFields)
      .then(function(response){
        validUserAccessToken = response.body.id;
        validUserId = response.body.userId;
        return chakram.wait();
      });
    });

    it("Have a UserProfile", function() {
      var url = "http://localhost:3000/api/users/"+validUserId+"/profile?access_token="+validUserAccessToken;
      validUserProfile = chakram.get(url);
      expect(validUserProfile).to.have.status(200);
      expect(validUserProfile).to.have.json('id',function(id){
        validUserProfileId = id;
      });
      return chakram.wait();
    });

    it("Can update their UserProfile", function() {
      var changedField = Date.now().toString();
      var validUserProfileFields = {
        id: validUserProfileId,
        handle:validUserProfile.handle,
        city: changedField
      };
      var url = "http://localhost:3000/api/users/"+validUserId+"/profile?access_token="+validUserAccessToken;
      var updatedUserProfile = chakram.put(url,validUserProfileFields);
      expect(updatedUserProfile).to.have.status(200);
      expect(updatedUserProfile).to.have.json('city',changedField);
      return chakram.wait();
    });

    after(function() {
      chakram.post("http://localhost:3000/api/users/logout?access_token="+validUserAccessToken);
      return chakram.wait();
    });
  });

  describe("Anybody", function() {
    var validUserAccessToken = "";
    var timestamp = Date.now().toString();
    var username = 'testuser-'+timestamp;
    var newUserId = "";
    var password = "password";

    it("can create a new user account", function() {
      var userFields = {
        username: username,
        password: password,
        email: username+"@gmail.com",
        handle: username
      };

      var validUserFields = {
        username: username,
        password: password
      };

      return chakram.post("http://localhost:3000/api/users/register",userFields)
      .then(function registerUserSuccess(response){
        newUserId = response.body.user.id;
        expect(response).to.have.status(200);
        expect(response).to.have.json('user.username',username);
        expect(response).to.have.json('user.profile',function checkUserProfile(profile){
          expect(profile).to.be.a('object');
        });
        return chakram.post("http://localhost:3000/api/users/login",validUserFields);
      }, function registerUserError(error){
        console.log('registerUserError, error is ', error);
        return chakram.wait();
      })
      .then(function loginUserSuccess(response){
        expect(response).to.have.status(200);
        validUserAccessToken = response.body.id;
        var url  = "http://localhost:3000/api/users/" + newUserId + "?access_token="+validUserAccessToken;
        return chakram.delete(url);
      }, function loginUserError(error){
        console.log('loginUserError, error is ', error);
        return chakram.wait();
      })
      .then(function deleteUserSuccess(response){
        expect(response).to.have.status(204);
        chakram.post("http://localhost:3000/api/users/logout?access_token="+validUserAccessToken);
        return chakram.wait();
      }, function deleteUserError(error){
        console.log('deleteUserError, error is ', error);
        return chakram.wait();
      });
    });
  });
});
